import praw
import re
import flask
import mysql.connector
from datetime import date
from time import sleep
import os


def env_to_bool(var):
    if var.lower() in ('true', 't', 'yes', 'y', '1'):
        return True
    return False


DB_HOST = os.getenv('DB_HOST', 'mysql')
DB_PORT = os.getenv('DB_PORT', 3306)
REDDIT_TOKEN = os.getenv('REDDIT_TOKEN', 'false')
REDDIT_SECRET = os.getenv('REDDIT_SECRET', 'false')
DRY_RUN = env_to_bool(os.getenv('DRY_RUN', 'true'))


def is_valid_subreddit(subreddit):
    valid = True
    if len(subreddit) > 20:
        valid = False
    return valid


def db_add_comment(comment):
    cnx = mysql.connector.connect(
        user='root',
        password='UZcLZVUXcF37gW',
        database='snafubot',
        host=DB_HOST,
        port=DB_PORT,
    )
    cursor = cnx.cursor()
    regex = r'r\/([a-zA-Z0-9_]+)'
    comment_id = comment.id
    post_id = comment.link_id[3:]
    subreddits = []
    date_created = date.today()

    # Search for subreddits in own comment
    matches = re.findall(regex, comment.body)
    if matches is not None:
        for match in matches:
            if len(match) < 21:
                subreddits.append(match.lower())

    # Insert comment into db
    if len(subreddits) > 0:
        query = "INSERT IGNORE INTO comments (id, post_id, subreddits, date_created) " \
                "VALUES('%s', '%s', '%s', '%s');" % (comment_id, post_id, ','.join(subreddits), date_created)
        print(query)
        cursor.execute(query)

    # Close db connection
    cursor.close()
    cnx.commit()
    cnx.close()


def main(*a, **ka):
    reddit = praw.Reddit(
        client_id='_c3plxJ7SZaeMQ',
        client_secret=REDDIT_SECRET,
        user_agent='python',
        username='SnafuBot',
        redirect_uri='http://localhost:8080',
        refresh_token=REDDIT_TOKEN,
    )
    regex = r'r\/([a-zA-Z0-9_]+)'

    # Process submission
    coaxedintoasnafu = reddit.subreddit('coaxedintoasnafu')
    regex = r'(r\/[a-zA-Z0-9_]+)'
    post_template = "Links to subreddits in flair or title: " \
                    "\n %s \n ***** \n" \
                    "^^^^I ^^^^am ^^^^a ^^^^bot. ^^^^PM ^^^^me " \
                    "^^^^if ^^^^you ^^^^have ^^^^any ^^^^issues ^^^^or ^^^^suggestions."

    for submission in coaxedintoasnafu.new(limit=5):
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print("Title: %s  :::  Flair: %s" % (submission.title, submission.link_flair_text))
        flair = submission.link_flair_text
        title = submission.title
        subs = []
        comment_str = ""

        # Search for subreddits in flair or title
        if flair is not None:
            matches = re.findall(regex, flair)
            if matches is not None:
                for match in matches:
                    subs.append(match)

        if len(subs) is 0:
            matches = re.findall(regex, title)
            if matches is not None:
                for match in matches:
                    subs.append(match)

        # If subreddits were found in flair or title
        if len(subs) > 0:
            print("Subreddits found.")
            if len(subs) == 1:
                if subs[0].lower() == 'r/coaxedintoasnafu':
                    # Skip this post to not tag own sub.
                    continue
            for sub in subs:
                comment_str += "\n%s\n" % sub

            # Check if already commented
            commented = False
            for comment in submission.comments:
                if comment.author == "SnafuBot":
                    commented = True
                    break

            if not commented:
                print("NOT COMMENTED:")
                print("==========================================================")
                print(post_template % comment_str)
                print("==========================================================")

                # TODO new_comment contains either comment or None.  Get id and insert into database. (Make sure to execute database command when processing each post, so that I can check the db for if I commented)
                if not DRY_RUN:
                    new_comment = submission.reply(post_template % comment_str)
                    db_add_comment(new_comment)
                    print(f'New comment: {new_comment}')
                else:
                    print('Dry run on.  Will not post comment.')
                break
            else:
                print("Commented already.")
        sleep(5)

    resp = flask.Response("Success.")
    resp.status_code = 200
    return resp


if __name__ == '__main__':
    main()
#main()



